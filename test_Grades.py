#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = missing-docstring

# --------------
# test_Grades.py
# --------------

# -------
# imports
# -------

import unittest  # main, TestCase

import Grades

# ----------------
# test_grades_eval
# ----------------


class test_grades_eval(unittest.TestCase):
    # def test_0(self) -> None:
    #     l_l_scores: list[list[int]] = [[0, 0, 0, 0, 0]]
    #     letter: str = Grades.grades_eval(l_l_scores)
    #     self.assertEqual(letter, "B-")
    def test_count_1(self) -> None:
        gradeList = [2, 2, 2, 2, 2]
        score = Grades.count_totals(gradeList)
        self.assertEqual(score, 5)

    def test_count_2(self) -> None:
        gradeList = [3, 3, 3, 3, 1, 1, 2, 2, 2, 2, 2, 2]
        score = Grades.count_totals(gradeList)
        self.assertEqual(score, 12)

    def test_count_3(self) -> None:
        gradeList = [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1]
        score = Grades.count_totals(gradeList)
        self.assertEqual(score, 0)

    def test_count_4(self) -> None:
        gradeList = [3, 3, 2, 3, 2, 3, 0, 0, 0, 1, 3, 3, 3, 3]
        score = Grades.count_totals(gradeList)
        self.assertEqual(score, 11)

    def test_count_5(self) -> None:
        gradeList = [0] * 0 + [1] * 6 + [2] * 26 + [3] * 10
        score = Grades.count_totals(gradeList)
        self.assertEqual(score, 41)

    def test_calc_1(self) -> None:
        grading = [5, 5, 4, 4, 4, 4, 4, 4, 3, 3, 3, 0]
        grade = Grades.calc_grade(grading, 4)
        self.assertEqual(grade, 2)

    def test_calc_2(self) -> None:
        grading = [11, 11, 10, 10, 10, 9, 9, 8, 8, 8, 7, 0]
        grade = Grades.calc_grade(grading, 11)
        self.assertEqual(grade, 0)

    def test_calc_3(self) -> None:
        grading = [13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8, 0]
        grade = Grades.calc_grade(grading, 10)
        self.assertEqual(grade, 6)

    def test_calc_4(self) -> None:
        grading = [13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8, 0]
        grade = Grades.calc_grade(grading, 7)
        self.assertEqual(grade, 11)

    def test_calc_5(self) -> None:
        grading = [39, 38, 37, 35, 34, 32, 31, 29, 28, 27, 25, 0]
        grade = Grades.calc_grade(grading, 36)
        self.assertEqual(grade, 3)

    def test_1(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 2, 0],
            [2, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 0],
            [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3],
            [2, 2, 2, 1, 2, 2, 2, 2, 2, 3, 2, 3, 0, 0],
            [0] * 8 + [1] * 1 + [2] * 29 + [3] * 4,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B-")

    def test_2(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 2],
            [2, 2, 2, 2, 2, 1, 1, 1, 3, 3, 3, 3, 3, 3],
            [0] * 0 + [1] * 0 + [2] * 42 + [3] * 0,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A")

    def test_3(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 0, 0],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 2],
            [2, 2, 2, 2, 2, 1, 1, 1, 3, 3, 3, 3, 3, 3],
            [0] * 15 + [1] * 0 + [2] * 27 + [3] * 0,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D")

    def test_4(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 2],
            [2, 2, 2, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3],
            [0] * 0 + [1] * 0 + [2] * 0 + [3] * 42,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B+")

    def test_5(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 1, 0, 0],
            [2, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 0],
            [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3],
            [2, 2, 2, 1, 2, 2, 2, 2, 2, 3, 2, 3, 3, 3],
            [0] * 0 + [1] * 0 + [2] * 42 + [3] * 0,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "F")

    def test_6(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [0] * 0 + [1] * 0 + [2] * 42 + [3] * 0,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A")

    def test_7(self) -> None:
        l_l_scores: list[list[int]] = [
            [0] * 5,
            [0] * 12,
            [0] * 14,
            [0] * 14,
            [0] * 42,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "F")

    def test_8(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 1],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [0] * 0 + [1] * 0 + [2] * 42 + [3] * 0,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B+")

    def test_9(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [0] * 11 + [1] * 5 + [2] * 16 + [3] * 10,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "C")

    def test_10(self) -> None:
        l_l_scores: list[list[int]] = [
            [2, 2, 2, 2, 0],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [0] * 0 + [1] * 0 + [2] * 42 + [3] * 0,
        ]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B+")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
