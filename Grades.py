#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = line-too-long

# ---------
# Grades.py
# ---------

# https://docs.python.org/3.6/library/functools.html
# https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html


# count_totals: takes in a list of grades and counts the
# number of 2/3s and also takes into account two 3s making
# up for a 1, returning the total calculated score
def count_totals(grades: list[int]) -> int:
    assert grades
    ones = 0
    passes = 0
    threes = 0
    for grade in enumerate(grades):
        if grade[1] == 1:
            ones += 1
        if grade[1] >= 2:  # grade counts as score
            passes += 1
        if grade[1] == 3:
            threes += 1
    # number of ones that have been made up by 2 3s
    total = passes + min(ones, threes // 2)
    assert total >= 0
    return total


def calc_grade(grading: list[int], score: int) -> int:
    assert grading and score >= 0
    final_grade = 12  # out of bounds of grading scale
    for index, grade in enumerate(grading):
        if grade <= score:
            # final grade is  the lowest index (highest) grade
            final_grade = min(final_grade, index)
    assert final_grade < 12
    return final_grade


# -----------
# grades_eval
# -----------
def grades_eval(l_l_scores: list[list[int]]) -> str:
    assert l_l_scores
    grade = 0

    # Grading scale
    result = ["A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F"]

    projects = l_l_scores[0]
    assert len(projects) == 5  # assert number of projects is correct
    count = count_totals(projects)
    grade = max(grade, calc_grade([5, 5, 4, 4, 4, 4, 4, 4, 3, 3, 3, 0], count))
    # grade is the lowest grade among the sections, meaning the maximum index returned by all runs of calc_grade

    exercises = l_l_scores[1]
    assert len(exercises) == 12
    count = count_totals(exercises)
    grade = max(grade, calc_grade([11, 11, 10, 10, 10, 9, 9, 8, 8, 8, 7, 0], count))

    blogs = l_l_scores[2]
    assert len(blogs) == 14
    count = count_totals(blogs)
    grade = max(grade, calc_grade([13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8, 0], count))

    papers = l_l_scores[3]
    assert len(papers) == 14
    count = count_totals(papers)
    grade = max(grade, calc_grade([13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8, 0], count))

    quizzes = l_l_scores[4]
    assert len(quizzes) == 42
    count = count_totals(quizzes)
    grade = max(
        grade, calc_grade([39, 38, 37, 35, 34, 32, 31, 29, 28, 27, 25, 0], count)
    )

    # assert in bounds
    assert result[grade]
    return result[grade]
