# CS373: Software Engineering Grades Repo

* Name: Raymond Wang

* EID: rmw2945

* GitLab ID: raymww

* HackerRank ID: raymond_m_wang

* Git SHA: 21fb7e7ecd6b98c4e11924bbb07c81982a480caa

* GitLab Pipelines: https://gitlab.com/raymww/cs373-grades/-/pipelines

* Estimated completion time: 6

* Actual completion time: 5

* Comments: n/a
